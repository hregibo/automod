import { MessageQueue } from '@hregibo/message-queue'
import { Client, IParsedMessage, IRC_TYPES } from 'twsc'
import { forbiddenWordsList } from './forbidden_words'

// Queue for messages to be sent
MessageQueue.DEFAULT_QUEUE_POLLING_MS = 1000
export const messageQueue = new MessageQueue()

export const joinChannels = (client: Client, channels: string[]) => {
  for (const elem of channels) {
    client.send(`JOIN #${elem}`)
  }
}

export const createQueues = (channels: string[]) => {
  for (const elem of channels) {
    messageQueue.setQueue({
      name: `#${elem}`
    })
  }
}

export const isForbidden = (message: IParsedMessage) => {
  if (message.type !== IRC_TYPES.PRIVMSG) return false
  return forbiddenWordsList
    .some(word => message
      .content?.toLowerCase()
      .includes(word.toLowerCase())
    )
}

export const banOwnerOfMessage = async (message: IParsedMessage) => {
  const owner = message.prefix.get('user')
  const reason = 'Automatic ban / Forbidden word used (can be an error)'
  messageQueue.enqueue({
    message: `.ban ${owner} ${reason}`,
    queue: message.params[0],
    priority: 1
  })
}
