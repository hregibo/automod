FROM node:lts-slim

RUN apt update; apt install -y python3 make
WORKDIR /home/node

COPY . .
RUN npm install
RUN npm run build

RUN apt remove -y make
RUN chown node:node -R .

USER node
ENTRYPOINT [ "node", "/home/node/build/index.js" ]
