import { IQueueMessage } from '@hregibo/message-queue'
import { createLogger } from 'bunyan'
import { Client, IRC_TYPES } from 'twsc'
import * as Bot from './bot'

const logger = createLogger({
  name: 'automod',
  stream: process.stdout,
  level: 'info'
})

const channels = [
  'lumikkode',
  'elkinoo',
  'foxamt',
  'farconer',
  'jamiemsm',
  'kwinu',
  'r_4_z_i_e_l',
  'vehviz',
  'und3r_achi3ver',
  'zlyx93'
]

const stop = (client: Client) => {
  logger.info('Terminating client and message queue, please wait...')
  client.emit('stop')
  Bot.messageQueue.emit('stop')
}

(async () => {
  // prepare MessageQueue
  Bot.createQueues(channels)

  // Create client & setup listeners
  const client = new Client({
    capabilities: [
      'twitch.tv/tags'
    ],
    hostname: 'wss://irc-ws.chat.twitch.tv',
    nickname: process.env.TWITCH_USERNAME,
    password: process.env.TWITCH_PASSWORD,
    reconnect: true
  })
  client.on('message', (message) => {
    if (message.type !== IRC_TYPES.PRIVMSG) {
      return false
    }
    if (Bot.isForbidden(message)) {
      logger.info(`${message.prefix.get('user')}@${message.params[0]} > ${message.content}`)
      Bot.banOwnerOfMessage(message)
    }
  })
  client.on('send', msg => {
    if (msg === 'PING' || msg.startsWith("PONG")) {
      return;
    }
    logger.warn(msg)    
  })
  client.on('ready', () => {
    logger.info('Ready, joining channels')
    Bot.joinChannels(client, channels)
    Bot.messageQueue.emit('start')
  })
  client.on('closed', () => {
    logger.error('Connection was closed')
    Bot.messageQueue.emit('stop')
  })

  Bot.messageQueue.on('dequeue', (message: IQueueMessage) => {
    client.send(`PRIVMSG ${message.queue} :${message.message}`)
  })

  process.on('SIGINT', () => stop(client))
  client.emit('start')
})()
